# NoSQL mongoDB

This repository contains an analysis about the online whiskey auction market using mongoDB and Python. The code was written in Jupyter Notebook and exported as a PDF. 

Used libraries are among others:
* Pymongo (tools to work with mongoDB in Python)
* Requests (data collection via API)
* Pandas (data analysis)
* Plotly (data visualization)

